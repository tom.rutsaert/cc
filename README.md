# Dev Talks - Code Competition

This repo contains the challenges for the Doccle Dev Talks Code Competition.
These challenges are randomly copied from a random year and random day of the advent of code challenges.
The input and solution are the input and solution I (tom) got on my advent of code account.

## Rules
Solve the challenges in small groups (1,2 or 3 persons).
In a group there can not be 2 members of the same squad!

Although I do not say from which year and which day the challenge is, you can easily find it and thus also find the solutions. 
Please do not search for it and try to solve it yourself!

Other Than that, you are completely free to solve it:
* how: pen and paper/intellij/bash/...
* where: laptop/remote/cloud/...
* what language: COBOL/Excel/[BrainFuck](https://en.wikipedia.org/wiki/Brainfuck)/...

## Where to save your sublime perfect code.
If you want to save your code, feel free to make
* a fork this repo 
* a new project 
* a branch with your name or team name on this repo.

## How to win
2 ways to call yourself Doccle Code Champions (until the next Dev Talks Code Competition)
1. The persons in a group that can solve the challenges the quickest have the honor the call themselves Doccle Code Champions!
2. Solve the challenge with the least amount of chars (all types of spaces do not count)

--> There will be a Slack _Status_ that emphasizes that you are Doccle Code C
hampion.