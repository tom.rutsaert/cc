
# Solution

I sha512hashed the answers:

you can you use following java snippet to hash your own answer:

```
    public static String getSHA512Answer(String answer) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-512");
		byte[] bytes = md.digest(answer.getBytes(StandardCharsets.UTF_8));
		StringBuilder sb = new StringBuilder();
		for (byte aByte : bytes) {
			sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
```

* The solution part 1 is: `2d66d891a2f53ba85575b67a3486ab5fcb862a71cd6a0fda25f283bed06e9379a0527f20354ac9571619157d9db92ba8f21bd19fcf3435690fc2b407fc19444d`
* The solution part 2 is: `a27f5f156b15c1ad7b53ea790e50da377c313187bcba4f859b7a3668b23fe910c78aacbd6f29f49a54797c6e67aa2232d1e8eaa0db9a457cec7e5fb8c1cb75f2`






