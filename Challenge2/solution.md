
# Solution

I sha512hashed the answers:

you can you use following java snippet to hash your own answer:

```
    public static String getSHA512Answer(String answer) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-512");
		byte[] bytes = md.digest(answer.getBytes(StandardCharsets.UTF_8));
		StringBuilder sb = new StringBuilder();
		for (byte aByte : bytes) {
			sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
```

* The solution part 1 is: `a0b4ae496b1b0c6d18dfba4122f5ec6bb2b56d9eb1d7689e48bf0b6d6952ebb57c35177f244d6db5bdd36343284e5ff8f3d9e12bcb5217454b074df532512dcd`
* The solution part 2 is: `06254defe761d2afec6595d8218b577d47796c07f52baf4d89e2406678a6f536c3fa87b78e978fc63a4e85e37c18ba43fb02518dc65118c301c21933e7bbcc50`






